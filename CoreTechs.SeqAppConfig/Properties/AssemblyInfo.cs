﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CoreTechs.SeqAppConfig")]
[assembly: AssemblyDescription("Standard way of initializing Seq via App Config settings")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CoreTechs")]
[assembly: AssemblyProduct("CoreTechs.SeqAppConfig")]
[assembly: AssemblyCopyright("Copyright © 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0a212c8a-7a76-43d8-88f1-d550324e56d1")]

[assembly: AssemblyVersion("0.3.*")]
[assembly: AssemblyInformationalVersion("0.3.1")]
[assembly: AssemblyFileVersion("0.3.0.0")]
