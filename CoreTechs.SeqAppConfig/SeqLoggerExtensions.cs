﻿using System;
using System.IO;
using System.Linq;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using SimpleConfig;

namespace CoreTechs.SeqAppConfig
{
    public static class SeqLoggerExtensions
    {
        public delegate string BufferPathSpec(string seqUniqueId);

        public static ILogger CreateFromSeqConfig(this LoggerConfiguration logBuilder, BufferPathSpec bufferPathSpec)
        {
            if (bufferPathSpec == null) throw new ArgumentNullException(nameof(bufferPathSpec));
            var seqInstances = Configuration.Load<SeqConfig>()?.Instances
                               ?? Enumerable.Empty<SeqInstanceDescription>();
            var levelSwitch = new LoggingLevelSwitch(LogEventLevel.Verbose);

            logBuilder = logBuilder.MinimumLevel.ControlledBy(levelSwitch);
            
            LoggingLevelSwitch getLevelSwitchOnlyOnce()
            {
                var ls = levelSwitch; levelSwitch = null;
                return ls;
            }
            string ensureBufferExists(string seqUniqueId)
            {
                var path = bufferPathSpec(seqUniqueId);
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception e)
                {
                    Serilog.Debugging.SelfLog.WriteLine(
                        $"Could not create durable directory for seq: \"{path}\"\n" +
                        $"due to error:\n{e}");
                    return null;
                }
                return path;
            }
            logBuilder = seqInstances.Reverse().Aggregate(logBuilder,
                (current, seq) =>
                    current.WriteTo.Seq(seq.Url, apiKey: seq.ApiKey,
                        bufferBaseFilename: ensureBufferExists(seq.UniqueId),
                        controlLevelSwitch: seq.IsPrimaryControl ? getLevelSwitchOnlyOnce() : null));

            return logBuilder.CreateLogger();
        }
    }
}
