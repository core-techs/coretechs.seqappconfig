﻿using System.Xml.Serialization;

namespace CoreTechs.SeqAppConfig
{
    [XmlType("instance")]
    public class SeqInstanceDescription
    {
        public string Url { get; set; }
        public string ApiKey { get; set; }
        public string UniqueId { get; set; }
        public bool IsPrimaryControl { get; set; }
    }
}
