@echo off
FOR %%F IN (.\*.csproj) DO (
 set filename=%%F
 goto pack
)
echo "Could not find csproj"
goto end

:pack
nuget pack "%filename%" -Build -symbols -properties Configuration=Release

:end
