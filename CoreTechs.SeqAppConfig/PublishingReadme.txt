To publish the package to the proget feed, use the following command (after filling out Username/Password and version):

NuGet.exe push [nameOfProject].[Version].nupkg [Username]:[Password] -Source https://proget.core-techs.net/nuget/Default

Additionally you may specify the api key (which is what [Username]:[Password] serves as) ahead of time using the `nuget setapikey` command: https://docs.microsoft.com/en-us/nuget/tools/nuget-exe-cli-reference#setapikey
