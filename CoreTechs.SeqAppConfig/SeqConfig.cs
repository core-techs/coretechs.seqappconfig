﻿using System.Collections.Generic;

namespace CoreTechs.SeqAppConfig
{
    public class SeqConfig
    {
        public List<SeqInstanceDescription> Instances { get; set; }
    }
}
